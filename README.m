clc ;
clear all;
close all;

 %reading input image should be not more than 100x100 resolution elements
 input = imread('C:/Users/Srikanth/Desktop/64m7AlA.jpg');
 figure
 imshow(input);
 
 %converting input color image to gray scale image
 monochrome = rgb2gray(input);
 figure
 imshow(monochrome);
 imwrite(monochrome,'C:/Users/Srikanth/Desktop/monochrome.jpg','jpg');

 % reading the size of the gary scale images rows and col's
 [row col] = size(monochrome);
 
 %coverting 2D gray scale image to columon vector 
 X = monochrome(:);
 
  % rowxcol input imgae dimensions %
 % pxq output imgae dimensions %
 %decimating by 2
 % row = m*p , col = n*q
 p = row/3;
 q = col/3;
 m=row/p;
 n=col/q;
 
  % creating Decimated matrix of pqxmn
 [i,j,k,l] = ndgrid(1:m,1:n,1:p,1:q);
 r = p*(l-1) + k;
 s = (m*n*p)*(l-1) + m*(k-1) + (m*p)*(j-1) + i;
 r = squeeze(reshape(r,[],1,1,1)); % Convert r and s to col.
 s = squeeze(reshape(s,[],1,1,1)); % vectors if necessary
 D = sparse(r,s,1,p*q,m*n*p*q);%decimating size matrix of pq by mnpq
 

 
 %defining blur kernal here it is meadian or average filter
 H_K = [1,2,1;2,4,2;1,2,1];
 H_K = H_K./16 ;
 % T is convolution matrix of blur kernal H for the image size row col
 T = convmtx2(H_K,row,col);
 H=[];
 n=0;
  z = row+3;
 for ii = 1:col
  H_f = T(z+n+1:z+n+row,: );
  H = [H;H_f];
  n=2*ii;
  z= z+row;
 end
 

% count/Tx_size is no of pixels that should shift right
count = 0;
warp_sparse_vec1 = ((row*count)+1) : (row *col);
warp_sparse_vec2 =  1 : (row*col - row*count) ;
W1 = sparse(warp_sparse_vec1,warp_sparse_vec2,1,row*col,row*col);

count = 10;
warp_sparse_vec1 = ((row*count)+1) : (row *col);
warp_sparse_vec2 =  1 : (row*col - row*count) ;
W2 = sparse(warp_sparse_vec1,warp_sparse_vec2,1,row*col,row*col);

count = 20;
warp_sparse_vec1 = ((row*count)+1) : (row *col);
warp_sparse_vec2 =  1 : (row*col - row*count) ;
W3 = sparse(warp_sparse_vec1,warp_sparse_vec2,1,row*col,row*col);

count = 30;
warp_sparse_vec1 = ((row*count)+1) : (row *col);
warp_sparse_vec2 =  1 : (row*col - row*count) ;
W4 = sparse(warp_sparse_vec1,warp_sparse_vec2,1,row*col,row*col);

count = 40;
warp_sparse_vec1 = ((row*count)+1) : (row *col);
warp_sparse_vec2 =  1 : (row*col - row*count) ;
W5 = sparse(warp_sparse_vec1,warp_sparse_vec2,1,row*col,row*col);

count = 50;
warp_sparse_vec1 = ((row*count)+1) : (row *col);
warp_sparse_vec2 =  1 : (row*col - row*count) ;
W6 = sparse(warp_sparse_vec1,warp_sparse_vec2,1,row*col,row*col);

count = 60;
warp_sparse_vec1 = ((row*count)+1) : (row *col);
warp_sparse_vec2 =  1 : (row*col - row*count) ;
W7 = sparse(warp_sparse_vec1,warp_sparse_vec2,1,row*col,row*col);

count = 70;
warp_sparse_vec1 = ((row*count)+1) : (row *col);
warp_sparse_vec2 =  1 : (row*col - row*count) ;
W8 = sparse(warp_sparse_vec1,warp_sparse_vec2,1,row*col,row*col);

count = 80;
warp_sparse_vec1 = ((row*count)+1) : (row *col);
warp_sparse_vec2 =  1 : (row*col - row*count) ;
W9 = sparse(warp_sparse_vec1,warp_sparse_vec2,1,row*col,row*col);

count = 90;
warp_sparse_vec1 = ((row*count)+1) : (row *col);
warp_sparse_vec2 =  1 : (row*col - row*count) ;
W10 = sparse(warp_sparse_vec1,warp_sparse_vec2,1,row*col,row*col);
%covlution or multiplcation of warp matrix with input image
Y1 = (1/m*n)*D*H*W1*double(X);
Y2 = (1/m*n)*D*H*W2*double(X);
Y3 = (1/m*n)*D*H*W3*double(X);
Y4 = (1/m*n)*D*H*W4*double(X);
Y5 = (1/m*n)*D*H*W5*double(X);
Y6 = (1/m*n)*D*H*W6*double(X);
Y7 = (1/m*n)*D*H*W7*double(X);
Y8 = (1/m*n)*D*H*W8*double(X);
Y9 = (1/m*n)*D*H*W9*double(X);
Y10 = (1/m*n)*D*H*W10*double(X);


imwrite(mat2gray(reshape(Y1,p,q)),'C:/Users/Srikanth/Desktop/input1.jpg','jpg');

imwrite(mat2gray(reshape(Y2,p,q)),'C:/Users/Srikanth/Desktop/input2.jpg','jpg');

imwrite(mat2gray(reshape(Y3,p,q)),'C:/Users/Srikanth/Desktop/input3.jpg','jpg');

imwrite(mat2gray(reshape(Y4,p,q)),'C:/Users/Srikanth/Desktop/input4.jpg','jpg');

imwrite(mat2gray(reshape(Y5,p,q)),'C:/Users/Srikanth/Desktop/input5.jpg','jpg');

imwrite(mat2gray(reshape(Y6,p,q)),'C:/Users/Srikanth/Desktop/input6.jpg','jpg');

imwrite(mat2gray(reshape(Y7,p,q)),'C:/Users/Srikanth/Desktop/input7.jpg','jpg');

imwrite(mat2gray(reshape(Y8,p,q)),'C:/Users/Srikanth/Desktop/input8.jpg','jpg');

imwrite(mat2gray(reshape(Y9,p,q)),'C:/Users/Srikanth/Desktop/input9.jpg','jpg');

imwrite(mat2gray(reshape(Y10,p,q)),'C:/Users/Srikanth/Desktop/input10.jpg','jpg');


R = W1'*H'*D'*D*H*W1 ;
R = R + ( W2'*H'*D'*D*H*W2);
R = R + ( W3'*H'*D'*D*H*W3);
R = R + ( W4'*H'*D'*D*H*W4);
R = R + ( W5'*H'*D'*D*H*W5);
R = R + ( W6'*H'*D'*D*H*W6);
R = R + ( W7'*H'*D'*D*H*W7);
R = R + ( W8'*H'*D'*D*H*W8);
R = R + ( W9'*H'*D'*D*H*W9);
R = R + ( W10'*H'*D'*D*H*W10);

% lhs =[ D*H*W1;(D*H*W2);(D*H*W3);(D*H*W4);(D*H*W5);(D*H*W6);(D*H*W7);(D*H*W8);(D*H*W9);(D*H*W10)];

% figure ; imshow(mat2gray(reshape(R,row,col)));


P = W1'*H'*D' *Y1 ;
P = P+(W2'*H'*D' *Y2);
P = P+(W3'*H'*D' *Y3);
P = P+(W4'*H'*D' *Y4);
P = P+(W5'*H'*D' *Y5);
P = P+(W6'*H'*D' *Y6);
P = P+(W7'*H'*D' *Y7);
P = P+(W8'*H'*D' *Y8);
P = P+(W9'*H'*D' *Y9);
P = P+(W10'*H'*D' *Y10);
 
% rhs = [Y1;Y2;Y3;Y4;Y5;Y6;Y7;Y8;Y9;Y10];

 intialGuess =zeros(row,col);
 intialGuess =intialGuess(:);

   maxIter = 500;
   iter = 0;
   eps = 0.01;
   

   res = intialGuess+eps*[(P - R * intialGuess)];
   mse = res' * res;
   mse0 = mse;
   while (iter < maxIter) %&& mse > eps^2 * mse0)
        res =(eps*[(P - R * intialGuess)]) ;
        intialGuess = intialGuess + res;
        mse = res' * res;
       fprintf(1, 'Gradient Descent Iteration %d mean-square error %3.3f\n', iter, mse);
       iter = iter + 1;
   end
  
 figure
 imshow(mat2gray(reshape(intialGuess,row,col)));



